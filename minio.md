

# minio简介

中文官方地址：http://www.minio.org.cn/



# MinIO安装以及使用

### 1.安装MinIO
MinIO的安装方式有很多，支持docker容器安装、二进制方式安装，这里我选择docker安装方式，docker环境默认已安装，docker可参考，本篇主要学习安装、以及上传图片。

#### 1.1 拉取MinIO镜像

```
docker pull minio/minio
```

#### 1.2 查看镜像

```
docker images
```

#### 1.3 运行容器

创建目录：一个用来存放配置，一个用来存储上传文件的目录。

```
mkdir -p /data/minio/config
mkdir -p /data/minio/data
```

这里的 \ 指的是命令还没有输入完，还需要继续输入命令，先不要执行的意思。
这里的9090端口指的是minio的客户端端口。虽然设置9090，但是我们在访问9000的时候，他会自动跳到9090

MINIO_ACCESS_KEY ：账号
MINIO_SECRET_KEY ：密码

```
docker run -p 9000:9000 -p 9090:9090 \
     --net=host \
     --name minio \
     -d --restart=always \
     -e "MINIO_ACCESS_KEY=minioadmin" \
     -e "MINIO_SECRET_KEY=minioadmin" \
     -v /data/minio/data:/data \
     -v /data/minio/config:/root/.minio \
     minio/minio server \
     /data --console-address ":9090" -address ":9000"
```

```
[root@iZ2vc39bx237teuxnraqm5Z ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                   PORTS                                          NAMES
c26718b5b4c1        minio/minio         "/usr/bin/docker-e..."   6 seconds ago       Up 6 seconds             0.0.0.0:9090->9000/tcp                         minio
[root@iZ2vc39bx237teuxnraqm5Z ~]# 
```

查看启动日志 docker logs -f '容器ID' 

#### 1.4 浏览器访问

http://47.98.43.31:9090

在输入控制打印的默认的AccessKey和SecretKey：

AccessKey: minioadmin       SecretKey: minioadmin



# Minio常见操作

- 官方demo: https://github.com/minio/minio-java